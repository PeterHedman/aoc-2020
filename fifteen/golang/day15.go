package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func readFile(file string) []int {
	var data []int

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), ",")
	for _, rawNr := range text {
		reg2 := regexp.MustCompile("[^0-9]+")
		str := reg2.ReplaceAllString(rawNr, "")
		val, e := strconv.Atoi(str)
		if e != nil {
			fmt.Println(e)
		}
		data = append(data, val)
	}
	return data
}

//starting numbers are unchanged
//following numbers are 0 if nr is new
// then if old the new nr to add is turn is the diff between the
// most recent times(iterations) it was spoken
// we need to fin the number that is in position 2020 for the given input
// 436 for the example data
func solution1(data []int) int {
	var previousNr = make(map[int]int)
	var newV int = data[0]
	var oldV int = 0

	for i := 0; i < 2020; i++ {
		oldV = newV

		if i < len(data) {
			newV = data[i]
		} else {
			_, ok := previousNr[oldV]
			if ok {
				diff := i - (previousNr[oldV] + 1)
				newV = diff
			} else {
				newV = 0
			}
		}
		previousNr[oldV] = i - 1
		//fmt.Println(oldV)
	}

	return newV
}

func solution2(data []int) int {
	var previousNr = make(map[int]int)
	var newV int = data[0]
	var oldV int = 0

	for i := 0; i < 30000000; i++ {
		oldV = newV

		if i < len(data) {
			newV = data[i]
		} else {
			_, ok := previousNr[oldV]
			if ok {
				diff := i - (previousNr[oldV] + 1)
				newV = diff
			} else {
				newV = 0
			}
		}
		previousNr[oldV] = i - 1
		//fmt.Println(oldV)
	}

	return newV
}

func main() {

	var data []int
	filePtr := flag.String("f", "", "a filepath string")
	preambleInt := flag.Int("p", 3, "device")
	flag.Parse()

	if *filePtr != "" && *preambleInt != 0 {
		data = readFile(*filePtr)
		fmt.Println(data)
		solution1 := solution1(data)
		fmt.Printf("Solution1: %d\n", solution1)
		solution2 := solution2(data)
		fmt.Printf("Solution2: %d\n", solution2)
	}
}
