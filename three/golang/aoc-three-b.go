package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n")
	for i := range text {
		str := strings.Trim(text[i], "\n")
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, str)
	}

	return data
}

func countCollisions(terrain []string, right int, down int) int {
	var collisions int = 0

	width := len(terrain[0])
	//fmt.Printf("elem: %d width: %d\n", elements, width)
	x := 0
	for y := down; y < len(terrain)-1; y += down {
		x += right
		pos := x % width
		seg := terrain[y]
		// fmt.Printf("pos: %d char: %c index: %d\n", pos, seg[pos], x)
		switch seg[pos] {
		case '#':
			collisions += 1
		case '.':
			continue
		}
	}
	return collisions
}

func main() {

	var terrain []string
	var collisions [5]int
	down := []int{1, 1, 1, 1, 2}
	right := []int{1, 3, 5, 7, 1}

	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		fmt.Printf("f = %s\n", *filePtr)
		terrain = readFile(*filePtr)
		//fmt.Printf("%v\n", terrain)
		mult := 1
		for i := 0; i < 5; i += 1 {
			collisions[i] = countCollisions(terrain, right[i], down[i])
			fmt.Printf("%d\n", collisions[i])
			mult = mult * collisions[i]
		}
		fmt.Printf("multiplied: %d\n", mult)
	}
}
