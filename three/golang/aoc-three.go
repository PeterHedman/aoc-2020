package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n")
	for i := range text {
		str := strings.Trim(text[i], "\n")
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, str)
	}

	return data
}

func countCollisions(terrain []string) int {
	var down int = 1
	var right int = 3
	var collisions int = 0

	width := len(terrain[0])
	//fmt.Printf("elem: %d width: %d\n", elements, width)
	x := 0
	for y := down; y < len(terrain)-1; y += down {
		x += right
		pos := x % width
		seg := terrain[y]
		// fmt.Printf("pos: %d char: %c index: %d\n", pos, seg[pos], x)
		switch seg[pos] {
		case '#':
			collisions += 1
		case '.':
			continue
		}
	}
	return collisions
}

func main() {

	var terrain []string
	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		fmt.Printf("f = %s\n", *filePtr)
		terrain = readFile(*filePtr)
		//fmt.Printf("%v\n", terrain)
		var collisions = countCollisions(terrain)
		fmt.Printf("%d\n", collisions)
	}
}
