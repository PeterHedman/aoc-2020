package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"regexp"
	"strconv"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n")
	for _, raw := range text {
		reg2 := regexp.MustCompile("[^a-zA-Z0-9]+")
		str := reg2.ReplaceAllString(raw, "")
		data = append(data, str)
	}
	return data[0 : len(data)-1]
}

type bPos struct {
	x       float64
	y       float64
	heading float64
}

type cPos struct {
	wp bPos
	sp bPos
}

func getString(order string) string {
	reg := regexp.MustCompile("[^a-zA-Z]+")
	str := reg.ReplaceAllString(order, "")
	return str
}

func getValue(order string) float64 {
	reg2 := regexp.MustCompile("[^0-9]+")
	str := reg2.ReplaceAllString(order, "")
	val, e := strconv.Atoi(str)
	if e != nil {
		fmt.Println(e)
	}
	return float64(val)
}

func calcHeading(current float64, change float64) float64 {
	val := current + change
	if val < 360 {
		return val
	}
	return val - 360
}

func calcNewPos(pos bPos, val float64) bPos {
	newPos := pos
	rad := pos.heading * (math.Pi / 180)
	newPos.x = newPos.x + (val * math.Sin(rad))
	newPos.y = newPos.y + (val * math.Cos(rad))
	return newPos
}

func getNewPosition(order string, pos bPos) bPos {
	newPos := pos
	ord := getString(order)
	val := getValue(order)
	switch ord {
	case "N":
		newPos.y = newPos.y + val
	case "S":
		newPos.y = newPos.y - val
	case "W":
		newPos.x = newPos.x - val
	case "E":
		newPos.x = newPos.x + val
	case "L":
		newPos.heading = calcHeading(newPos.heading, -val)
	case "R":
		newPos.heading = calcHeading(newPos.heading, val)
	case "F":
		newPos = calcNewPos(pos, val)
	default:
		fmt.Printf("Error: %s\n", ord)
	}
	return newPos
}

// starting heading is east x++
// Nr is movement in combination with heading or forward
// R and L means turn right and left number is here degrees
// F is the forward action in current heading
// Go through data and calculate manhattan distance in the end
// mDist = | x2 - x1| + |y2 -y1|
func solution1(data []string) float64 {
	var result float64 = 0
	currentPos := bPos{0, 0, 90}

	for _, order := range data {
		currentPos = getNewPosition(order, currentPos)
		//fmt.Println("x:%f y:%f head:%f", currentPos.x, currentPos.y, currentPos.heading)
	}

	result = math.Abs(currentPos.x) + math.Abs(currentPos.y)
	return result
}

//rotate the wp to new position
func changeWP(wp bPos, change float64) bPos {
	wp.heading = calcHeading(wp.heading, change)
	turns := int(math.Abs(change / 90))
	if change < 0 {
		for i := 0; i < turns; i++ {
			wp.x, wp.y = -wp.y, wp.x
		}

	} else {
		for i := 0; i < turns; i++ {
			wp.x, wp.y = wp.y, -wp.x
		}
	}

	return wp
}

func updateWpAngle(wP bPos) bPos {
	rad := wP.heading * (math.Pi / 180)
	wP.x = wP.x * math.Sin(rad)
	wP.y = wP.y * math.Cos(rad)
	wP.heading = 90 - math.Atan(wP.x/wP.y)*180/math.Pi
	return wP
}

// Boat will travel towards waypoint val nr of times
func calcNewPos2(pos cPos, val float64) cPos {
	//rad := pos.wp.heading * (math.Pi / 180)
	//low := pos.wp.x + pos.wp.y

	pos.sp.x = pos.sp.x + (val * pos.wp.x)
	pos.sp.y = pos.sp.y + (val * pos.wp.y)

	return pos
}

func getNewPosition2(order string, pos cPos) cPos {
	ord := getString(order)
	val := getValue(order)
	switch ord {
	case "N":
		pos.wp.y = pos.wp.y + val
	case "S":
		pos.wp.y = pos.wp.y - val
	case "W":
		pos.wp.x = pos.wp.x - val
	case "E":
		pos.wp.x = pos.wp.x + val
	case "L":
		pos.wp = changeWP(pos.wp, -val)
	case "R":
		pos.wp = changeWP(pos.wp, +val)
	case "F":
		pos = calcNewPos2(pos, val)
	default:
		fmt.Printf("Error: %s\n", ord)
	}
	return pos
}

// there is a waypoint position relative to the ship determining movement
// waypoint will alway be relative to ship and move with it
func solution2(data []string) float64 {
	var solution float64 = 0
	shipPos := bPos{0, 0, 90}
	degrees := float64(90)
	wayPoint := bPos{shipPos.x + 10, shipPos.y + 1, degrees}
	pos := cPos{wayPoint, shipPos}

	for _, order := range data {
		pos = getNewPosition2(order, pos)
		//fmt.Println(pos)
	}

	solution = math.Abs(pos.sp.x) + math.Abs(pos.sp.y)

	return solution
}

func main() {

	var data []string
	filePtr := flag.String("f", "", "a filepath string")
	preambleInt := flag.Int("p", 3, "device")
	flag.Parse()

	if *filePtr != "" && *preambleInt != 0 {
		data = readFile(*filePtr)
		//fmt.Println(data)
		solution1 := solution1(data)
		fmt.Printf("Solution1: %d\n", solution1)
		solution2 := solution2(data)
		fmt.Printf("Solution2: %d\n", solution2)
	}
}
