package main

import (
	"flag"
	"fmt"
	"image"
	"io/ioutil"
	"log"
	"regexp"
	"strings"
)

func readFile(file string) string {
	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	return string(content)
}

// this solution is based from https://github.com/deosjr/adventofcode2020/blob/master/24/day24.go

func mutate(data string) int {

	delta := map[string]image.Point{
		"e": {2, 0}, "se": {1, 1}, "sw": {-1, 1},
		"w": {-2, 0}, "nw": {-1, -1}, "ne": {1, -1},
	}

	black := map[image.Point]struct{}{}
	for _, s := range strings.Fields(data) {
		p := image.Point{0, 0}
		for _, s := range regexp.MustCompile(`(e|se|sw|w|nw|ne)`).FindAllString(s, -1) {
			p = p.Add(delta[s])
		}

		if _, ok := black[p]; ok {
			delete(black, p)
		} else {
			black[p] = struct{}{}
		}
	}
	return len(black)
}

func solution1(data string) int {
	var result int = 0

	result = mutate(data)
	//for y, s := range strings.
	// 0 floor, 1 free tile, 2 occupied tile
	//result = occupiedSeats
	return result
}

func mutate2(data string) int {

	delta := map[string]image.Point{
		"e": {2, 0}, "se": {1, 1}, "sw": {-1, 1},
		"w": {-2, 0}, "nw": {-1, -1}, "ne": {1, -1},
	}

	black := map[image.Point]struct{}{}
	for _, s := range strings.Fields(data) {
		p := image.Point{0, 0}
		for _, s := range regexp.MustCompile(`(e|se|sw|w|nw|ne)`).FindAllString(s, -1) {
			p = p.Add(delta[s])
		}

		if _, ok := black[p]; ok {
			delete(black, p)
		} else {
			black[p] = struct{}{}
		}
	}

	for i := 0; i < 100; i++ {
		neigh := map[image.Point]int{}
		for p := range black {
			for _, d := range delta {
				neigh[p.Add(d)]++
			}
		}

		new := map[image.Point]struct{}{}
		for p, n := range neigh {
			if _, ok := black[p]; ok && n == 1 || n == 2 {
				new[p] = struct{}{}
			}
		}
		black = new
	}
	return len(black)
}

func solution2(data string) int {
	var result int = 0
	//populate tile map
	result = mutate2(data)
	return result
}

func main() {

	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		data := readFile(*filePtr)
		//fmt.Printf("%v\n", data)
		solution1 := solution1(data)
		fmt.Printf("Solution1: %d\n", solution1)
		solution2 := solution2(data)
		fmt.Printf("Solution2: %d\n", solution2)
	}
}
