package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n")
	for _, raw_rule := range text {
		reg2 := regexp.MustCompile("[^A-Za-z0-9\\+\\-: ]+")
		str := reg2.ReplaceAllString(raw_rule, "")
		data = append(data, str)
	}
	return data
}

func getInstruction(instr string) string {
	var inS string
	str := strings.Split(instr, " ")
	inS = str[0]
	return inS
}

func getValue(instr string) int {
	str := strings.Split(instr, " ")
	val, e := strconv.Atoi(str[1])
	if e != nil {
		fmt.Println(e)
	}
	return val
}

func getValueString(instr string) string {
	str := strings.Split(instr, " ")
	return str[1]
}

// we should run the ex pgm and get expected results 5
func solution1(pgm []string, problem int) int {
	var accumulator int = 0
	runInstr := make(map[string]int)
	pc := 0
	runInstr[getInstruction(pgm[0])] = 1999
	for {
		instr := pgm[pc]
		//fmt.Printf("Accumulator: %d, PC: %d Len: %d", accumulator, pc, len(pgm))
		//fmt.Printf("Instruction: %s, seenInstr: %d\n", instr, runInstr[instr])
		if runInstr[instr] == pc && pc != 0 {
			if problem == 1 {
				return accumulator
			}
			//fmt.Printf("loop done\n")
			return -1
		}
		runInstr[instr] = pc
		switch getInstruction(instr) {
		case "acc":
			//increment or decrement accumulator
			accumulator += getValue(instr)
			pc++
		case "jmp":
			//jump to other instruction according to value
			val2 := getValue(instr)
			if val2 == 0 {
				fmt.Printf("error here\n")
				return -1
			}
			pc = pc + getValue(instr)
		case "nop":
			// no operation
			pc++
		default:
			// unknown instruction
		}
		if pc > len(pgm) || pc < 0 {
			fmt.Printf("Crash\n")
			return -1
		} else if pc == len(pgm)-1 {
			//fmt.Printf("true exit\n")
			return accumulator
		}
	}
	//fmt.Printf("true exit2\n")
	return accumulator
}

func solution2(pgm []string, problem int) int {
	var solution int = -1
	var moddedPgm = make([]string, len(pgm))
	// try replacing the nop or jmp instructions
	// one at a time and see if pgm works
	copy(moddedPgm, pgm)
	for i := 0; i < len(moddedPgm)-1; i++ {
		inS := moddedPgm[i]
		insStr := getInstruction(inS)
		valStr := getValueString(inS)
		val := getValue(inS)
		// reset pgm
		updates := false
		if strings.Compare(insStr, "nop") == 0 && val != 0 {
			moddedPgm[i] = "jmp " + valStr
			updates = true
		} else if strings.Compare(insStr, "jmp") == 0 {
			moddedPgm[i] = "nop " + valStr
			updates = true
		}
		if updates == true {
			//fmt.Printf("changed instruction %s to %s iter:%d\n", inS, moddedPgm[i], i)
			solution = solution1(moddedPgm, problem)
		}
		if solution != -1 {
			return solution
		}
		//restore pgm
		copy(moddedPgm, pgm)
	}
	return solution
}

func main() {

	var data []string
	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		fmt.Printf("f = %s\n", *filePtr)
		data = readFile(*filePtr)
		//fmt.Printf("%v\n", data)
		solution := solution1(data, 1)
		fmt.Printf("Solution1: %d\n", solution)
		solution = solution2(data, 2)
		fmt.Printf("Solution2: %d\n", solution)
	}
}
