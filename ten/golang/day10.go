package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

func readFile(file string) []int {
	var data []int

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n")
	for _, rawNr := range text {
		reg2 := regexp.MustCompile("[^0-9]+")
		str := reg2.ReplaceAllString(rawNr, "")
		val, e := strconv.Atoi(str)
		if e != nil {
			fmt.Println(e)
		}
		data = append(data, val)
	}
	return data[0 : len(data)-1]
}

func solution1(data []int) int {
	var result int = 0
	sort.Ints(data)
	cmp := 0
	diff1 := 0
	diff3 := 1
	for _, val := range data {
		if val-cmp == 1 {
			diff1++
			cmp = val
		} else if val-cmp == 3 {
			diff3++
			cmp = val
		}
	}
	result = diff1 * diff3
	return result
}

func checkSlice(before int, after int) bool {
	// i is removed is adaptervalid check previous
	if after-before <= 3 {
		return true
	}
	return false
}

func solution2(data []int) float64 {
	//for a sorted list one only needs to try and remove elements
	// adapter accepts n -3 jolts
	// Device must have 3 more than highest adapter
	// outlet has 0 jolts
	//last cannot be removed
	var canBeRemoved []int
	var canBeRemoved2 []int
	port := 0
	data = append(data, port)
	sort.Ints(data)
	// add 0 and last value +3
	//every removed adapter gives dimension of problem
	for i := 0; i < len(data)-2; i++ {
		if checkSlice(data[i], data[i+2]) {
			canBeRemoved = append(canBeRemoved, data[i+1])
		}
	}
	cbr := canBeRemoved
	for i := 0; i < len(cbr)-2; i++ {
		if checkSlice(cbr[i], cbr[i+2]) {
			canBeRemoved2 = append(canBeRemoved2, cbr[i+1])
		}
	}
	//series of one numbers were one can be removed 2 combinations each
	//series of three numbers were maximum one can be removed 7 combinations each
	seriesOfThree := len(canBeRemoved2)
	seriesOfOne := len(canBeRemoved) - seriesOfThree*3
	totalCombinations := math.Pow(7, float64(seriesOfThree)) * math.Pow(2, float64(seriesOfOne))
	return totalCombinations
}

func main() {

	var data []int
	filePtr := flag.String("f", "", "a filepath string")
	preambleInt := flag.Int("p", 3, "device")
	flag.Parse()

	if *filePtr != "" && *preambleInt != 0 {
		data = readFile(*filePtr)
		solution1 := solution1(data)
		fmt.Printf("Solution1: %d\n", solution1)
		solution2 := solution2(data)
		fmt.Printf("Solution2: %d\n", solution2)
	}
}
