package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n\n")
	for i := range text {
		str := strings.Replace(text[i], "\n", "", -1)
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, str)
	}

	return data
}

func removeDuplicates(data string) string {
	occured := make(map[rune]bool)
	list := []rune{}

	for _, char := range data {
		if _, value := occured[char]; !value {
			occured[char] = true
			list = append(list, char)
		}
	}
	return string(list)
}

func countYesResponses(responses []string) int {
	var yesResponses int = 0

	for _, response := range responses {
		response_filtered := removeDuplicates(response)
		yesResponses += len(response_filtered)
	}
	//fmt.Printf("data %v", data)
	return yesResponses
}

func main() {

	var data []string
	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		fmt.Printf("f = %s\n", *filePtr)
		data = readFile(*filePtr)
		fmt.Printf("%v\n", data)
		solution := countYesResponses(data)
		fmt.Printf("solution: %d\n", solution)
	}
}
