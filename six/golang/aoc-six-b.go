package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content2, err := ioutil.ReadFile(file)
	// below is to fix bug with parsing of input data
	content := content2[0 : len(content2)-1]
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n\n")
	for i := range text {
		str := strings.Replace(text[i], "\n", "-", -1)
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, str)
	}
	return data
}

func removeDuplicates(data string) string {
	occured := make(map[rune]bool)
	list := []rune{}

	for _, char := range data {
		if _, value := occured[char]; !value {
			occured[char] = true
			list = append(list, char)
		}
	}
	return string(list)
}

func keepDuplicates(data string, occurances int) string {
	occured := make(map[rune]int)
	list := []rune{}
	// go initializes the int values to 0
	for _, char := range data {
		if occured[char] < occurances {
			occured[char]++
		}
		if occured[char] >= occurances {
			list = append(list, char)
		}
	}

	return string(list)
}

func countYesResponses(responses []string) int {
	var yesResponses int = 0

	for _, response := range responses {
		response_filtered := removeDuplicates(response)
		//fmt.Printf("%s\n", response_filtered)
		yesResponses += len(response_filtered)
	}
	return yesResponses
}

func countYesResponses4All(responses []string) int {
	var yesResponsesAll int = 0
	var newList []string

	// only keep duplicates from groups in new list
	for _, group := range responses {
		persons := strings.Split(group, "-")
		//fmt.Printf("group: %v\n", persons)
		nrPersons := len(persons)
		newValue := keepDuplicates(group, nrPersons)
		newList = append(newList, newValue)
	}
	// fmt.Printf("%v\n", newList)
	// then do the same as before with new list
	yesResponsesAll = countYesResponses(newList)

	return yesResponsesAll
}

func main() {

	var data []string
	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		fmt.Printf("f = %s\n", *filePtr)
		data = readFile(*filePtr)
		fmt.Printf("%v\n", data)
		solution := countYesResponses4All(data)
		fmt.Printf("solution: %d\n", solution)
	}
}
