package main

import (
	"flag"
	"fmt"
	"image"
	"io/ioutil"
	"log"
	"regexp"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n")
	for _, raw := range text {
		reg2 := regexp.MustCompile("[^L.#]+")
		str := reg2.ReplaceAllString(raw, "")
		data = append(data, str)
	}
	return data
}

// this solution was taken from https://github.com/mnml/aoc/blob/master/2020/11/2.go
func mutate(seats map[image.Point]rune, maxS int, adj func(p, d image.Point) image.Point) int {
	var res int
	delta := []image.Point{{1, 1}, {1, 0}, {0, 1}, {1, -1}, {-1, 1}, {-1, 0}, {0, -1}, {-1, -1}}
	for diff := true; diff; {
		res, diff = 0, false
		next := map[image.Point]rune{}
		for x, r := range seats {
			sum := 0
			for _, y := range delta {
				if seats[adj(x, y)] == '#' {
					sum++
				}
			}

			if r == '#' && sum >= maxS {
				r = 'L'
			} else if r == 'L' && sum == 0 || r == '#' {
				r = '#'
				res++
			}
			next[x] = r
			diff = diff || next[x] != seats[x]
		}
		seats = next
	}
	return res
}

func solution1(data []string) int {
	var result int = 0
	seats := map[image.Point]rune{}
	//populate seat map
	for y, row := range data {
		for x, seat := range row {
			seats[image.Point{x, y}] = seat
		}
	}
	fmt.Println(seats)
	result = mutate(seats, 4, func(p, d image.Point) image.Point { return p.Add(d) })
	//for y, s := range strings.
	// 0 floor, 1 free seat, 2 occupied seat
	//result = occupiedSeats
	return result
}

func solution2(data []string) int {
	var result int = 0
	seats := map[image.Point]rune{}
	//populate seat map
	for y, row := range data {
		for x, seat := range row {
			seats[image.Point{x, y}] = seat
		}
	}
	fmt.Println(seats)
	result = mutate(seats, 5, func(p, d image.Point) image.Point {
		for seats[p.Add(d)] == '.' {
			p = p.Add(d)
		}
		return p.Add(d)
	})
	return result
}

func main() {

	filePtr := flag.String("f", "", "a filepath string")
	preambleInt := flag.Int("p", 3, "device")
	flag.Parse()

	if *filePtr != "" && *preambleInt != 0 {
		//fmt.Printf("f = %s\n", *filePtr)
		//fmt.Printf("p = %d\n", *preambleInt)
		data := readFile(*filePtr)
		fmt.Printf("%v\n", data)
		solution1 := solution1(data)
		fmt.Printf("Solution1: %d\n", solution1)
		solution2 := solution2(data)
		fmt.Printf("Solution2: %d\n", solution2)
	}
}
