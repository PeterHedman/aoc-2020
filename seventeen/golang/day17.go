package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n")
	for _, raw := range text {
		reg2 := regexp.MustCompile("[^\\.#]+")
		str := reg2.ReplaceAllString(raw, "")
		data = append(data, str)
	}
	return data
}

type Point [4]int

func (p Point) Add(q Point) Point {
	for i := range p {
		p[i] += q[i]
	}
	return p
}

// 3dimensions x,y,z
// state active or inactive
// active # inactive .
// neighbour influeces
// 26 neighbours 8 + 2 * 9
// Rules 2-3 neighbours active --> active
// neighbours < 3 reamin or become inactive
// 6 cycle of changes
// This solutin is based on https://github.com/deosjr/adventofcode2020/blob/master/17/day17.go
func mutate(grid map[Point]struct{}, dim, cycles int) int {
	for i := 0; i < cycles; i++ {
		neigh := map[Point]int{}
		for p := range grid {
			for _, d := range delta(dim)[1:] {
				neigh[p.Add(d)]++
			}
		}

		new := map[Point]struct{}{}
		for p, n := range neigh {
			if _, ok := grid[p]; ok && n == 2 || n == 3 {
				new[p] = struct{}{}
			}
		}
		grid = new
	}
	return len(grid)
}

func delta(dim int) (ds []Point) {
	if dim == 0 {
		return []Point{{}}
	}
	for _, v := range []int{0, 1, -1} {
		for _, p := range delta(dim - 1) {
			p[dim-1] = v
			ds = append(ds, p)
		}
	}
	return
}
func solution1(data []string) int {
	var res int = 0
	// populate map
	grid := map[Point]struct{}{}
	for y, s := range data {
		for x, r := range s {
			if r == '#' {
				grid[Point{x, y}] = struct{}{}
			}
		}
	}

	res = mutate(grid, 3, 6)
	return res
}

func solution2(data []string) int {
	var res int = 0
	grid := map[Point]struct{}{}
	//populate map
	for y, s := range data {
		for x, r := range s {
			if r == '#' {
				grid[Point{x, y}] = struct{}{}
			}
		}
	}
	// go through game of life
	res = mutate(grid, 4, 6)
	return res
}

func main() {

	var data []string
	filePtr := flag.String("f", "", "a filepath string")
	preambleInt := flag.Int("p", 3, "device")
	flag.Parse()

	if *filePtr != "" && *preambleInt != 0 {
		data = readFile(*filePtr)
		fmt.Println(data)
		solution1 := solution1(data)
		fmt.Printf("Solution1: %d\n", solution1)
		solution2 := solution2(data)
		fmt.Printf("Solution2: %d\n", solution2)
	}
}
