package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func readFile(file string) []string {
	var passwords []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	// Convert []byte to string and print to screen
	text := strings.Split(string(content), "\n")
	for i := range text {
		str := strings.Replace(text[i], ":", "", -1)
		passwords = append(passwords, str)
	}

	return passwords
}

func countValidPasswords(passwords []string) int {
	var validPasswords int = 0
	for i := 0; i < len(passwords)-1; i += 1 {
		// fmt.Printf("password string %v\n", passwords[i])
		passwordParts := strings.Split(passwords[i], " ")
		ranges := strings.Split(passwordParts[0], "-")
		min, err := strconv.Atoi(ranges[0])
		if err != nil {
			log.Fatal(err)
		}
		max, err := strconv.Atoi(ranges[1])
		if err != nil {
			log.Fatal(err)
		}
		character := passwordParts[1]
		pw := passwordParts[2]
		//fmt.Printf("ranges:%d to %d char: %s pw:%s\n", min, max, character, pw)
		if pw[min-1] == character[0] && pw[max-1] == character[0] {
			continue
		} else if pw[min-1] == character[0] || pw[max-1] == character[0] {
			validPasswords += 1
		} else {
			continue
		}
	}
	return validPasswords
}

func main() {

	var passwords []string
	//	var opt_counter int
	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		fmt.Printf("f = %s\n", *filePtr)
		passwords = readFile(*filePtr)
		fmt.Printf("%v\n", passwords)
		var validPasswords = countValidPasswords(passwords)
		fmt.Printf("%d\n", validPasswords)
	}
}
