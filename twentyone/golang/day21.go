package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"sort"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n")
	for _, rawNr := range text {
		reg2 := regexp.MustCompile("[^a-z\\(\\) ]+")
		str := reg2.ReplaceAllString(rawNr, "")
		data = append(data, str)
	}
	return data[0 : len(data)-1]
}

func getIngredients(data []string) []string {
	var ingr []string
	for _, meal := range data {
		str := strings.Split(meal, "(")
		str2 := strings.Split(str[0], " ")
		for _, str3 := range str2 {
			reg2 := regexp.MustCompile("[^a-z]+")
			str4 := reg2.ReplaceAllString(str3, "")
			if len(str4) > 1 {
				ingr = append(ingr, str4)
			}
		}
	}
	return ingr
}

func getUniqueList(data []string) []string {
	var occ = make(map[string]bool)
	var res []string
	for _, str := range data {
		if occ[str] == true {
			//not add
		} else {
			occ[str] = true
			res = append(res, str)
		}
	}
	return res
}

func getAllergens(data []string) []string {
	var aller []string
	for _, meal := range data {
		str := strings.Split(meal, "contains")
		str2 := strings.Split(str[1], " ")
		for _, str3 := range str2 {
			reg2 := regexp.MustCompile("[^a-z]+")
			str4 := reg2.ReplaceAllString(str3, "")
			if len(str4) > 1 {
				aller = append(aller, str4)
			}
		}
	}
	return aller
}

func removeAllergen(aller []string, allergen string) []string {
	var res []string
	for _, str := range aller {
		if !(strings.Compare(str, allergen) == 0) {
			res = append(res, str)
		}
	}
	return res
}

func getOccurenceOfIng(iList []string, ing string) int {
	var occ int = 0
	for _, ing2 := range iList {
		if strings.Compare(ing, ing2) == 0 {
			occ++
		}
	}
	return occ
}

func solution1(data []string) int {
	var res int = 0
	allerPerIng := make(map[string][]string)
	iList := getIngredients(data)
	//fmt.Println(iList)
	uList := getUniqueList(iList)
	aList := getAllergens(data)
	uaList := getUniqueList(aList)
	var noAllergens []string
	// get all possible allergens
	for _, meal := range data {
		for _, ing := range uList {
			for _, aller := range uaList {
				if strings.Contains(meal, ing) && strings.Contains(meal, " "+aller) {
					//this is a potential allergenic ingredient add if it is not already added
					allerPerIng[ing] = append(allerPerIng[ing], aller)
				}
			}
		}
	}

	for _, ing := range uList {
		allerPerIng[ing] = getUniqueList(allerPerIng[ing])
	}

	// remove non possible allergensby deducing that recepies that contains allergen without our suspected ing
	for _, meal := range data {
		for _, ing := range uList {
			for _, aller := range uaList {
				// regexp was tricky due to fish and shellfish hence the whitespace
				if !strings.Contains(meal, ing) && strings.Contains(meal, " "+aller) {
					//remove this allergen from this ingridient
					allerPerIng[ing] = removeAllergen(allerPerIng[ing], aller)
					if len(allerPerIng[ing]) == 0 {
						//fmt.Printf("No allergen for %s\n", ing)
						noAllergens = append(noAllergens, ing)
					}
				}
			}
		}
	}
	noAllergens = getUniqueList(noAllergens)
	occ := 0
	for _, ing := range noAllergens {
		occ += getOccurenceOfIng(iList, ing)
		//fmt.Printf("%s %d\n", ing, occ)
	}
	res = occ
	return res
}

// produce a , seperated list of foods that contains allergens
func solution2(data []string) string {
	var res string
	allerPerIng := make(map[string][]string)
	iList := getIngredients(data)
	//fmt.Println(iList)
	uList := getUniqueList(iList)
	aList := getAllergens(data)
	uaList := getUniqueList(aList)
	fmt.Println(uaList)
	// get all possible allergens
	for _, meal := range data {
		for _, ing := range uList {
			for _, aller := range uaList {
				if strings.Contains(meal, ing) && strings.Contains(meal, " "+aller) {
					//this is a potential allergenic ingredient add if it is not already added
					allerPerIng[ing] = append(allerPerIng[ing], aller)
				}
			}
		}
	}

	for _, ing := range uList {
		allerPerIng[ing] = getUniqueList(allerPerIng[ing])
	}

	// remove non possible allergensby deducing that recepies that contains allergen without our suspected ing
	for _, meal := range data {
		for _, ing := range uList {
			for _, aller := range uaList {
				// regexp was tricky due to fish/shellfish/nuts/peanuts hence the whitespace
				if !strings.Contains(meal, ing) && strings.Contains(meal, " "+aller) {
					//remove this allergen from this ingridient
					allerPerIng[ing] = removeAllergen(allerPerIng[ing], aller)
				}
			}
		}
	}

	// we still have ingredients with multiple allergens
	fAller := make(map[string]string)
	for i := 0; i < 40; i++ {
		remove := "not-set"
		// find ing with single allergen
		for ing, _ := range allerPerIng {
			//find ingredient with only 1 allergen then remove this allergen from others
			// repeat until all have only one
			if len(allerPerIng[ing]) == 1 {
				remove = allerPerIng[ing][0]
				fAller[ing] = remove
			}
		}
		for ing, _ := range allerPerIng {
			if len(allerPerIng[ing]) >= 2 {
				allerPerIng[ing] = removeAllergen(allerPerIng[ing], remove)
			}
		}
	}

	var ingWithAllergens []string
	//for ing, aller := range allerPerIng {
	//	if len(allerPerIng[ing]) >= 1 {
	//		str := aller[0] + " :" + ing + ","
	//		ingWithAllergens = append(ingWithAllergens, str)
	//	}
	//}
	for ing, aller := range fAller {
		str := aller + " :" + ing + ","
		ingWithAllergens = append(ingWithAllergens, str)
	} //fmt.Println(ingWithAllergens)
	sort.Strings(ingWithAllergens)
	fmt.Println(ingWithAllergens)

	for _, ing := range ingWithAllergens {
		str := strings.Split(ing, " :")
		res = res + str[1]
	}

	fmt.Println(res)
	return res[0 : len(res)-2]
}

func main() {

	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		data := readFile(*filePtr)
		fmt.Println(data)
		solution1 := solution1(data)
		fmt.Printf("Solution1: %d\n", solution1)
		solution2 := solution2(data)
		fmt.Printf("Solution2: %s\n", solution2)
	}
}
