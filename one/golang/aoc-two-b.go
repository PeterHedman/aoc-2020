package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
)

func readFile(filePath string) []int {
	var values []int

	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lineStr := scanner.Text()
		num, _ := strconv.Atoi(lineStr)
		values = append(values, num)
	}

	return values
}

func main() {

	var values []int
	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		fmt.Printf("f = %s\n", *filePtr)
		values = readFile(*filePtr)
		//fmt.Println("%v", values)
		for _, value := range values {
			for _, value2 := range values {
				for _, value3 := range values {
					var sum = value3 + value2 + value
					if sum == 2020 {
						var multiple = value3 * value2 * value
						fmt.Printf("wanted multiple is %d * %d * %d = %d\n", value, value2, value3, multiple)
						os.Exit(1)
					}
				}
			}
		}
		fmt.Printf("No values that sum to 2020 found in input data\n")
	}
}
