package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

var rex = regexp.MustCompile("(\\w+):(\\w+)")

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n\n")
	for i := range text {
		str := strings.Replace(text[i], "\n", " ", -1)
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, str)
	}

	return data
}

func validateString(str string, rg string, length int) bool {
	reS, err := regexp.MatchString(rg, str)
	if err != nil {
		log.Fatal(err)
	}
	if len(str) != length {
		reS = false
	}
	return reS
}

func validRange(value string, low int, high int) bool {
	numValue, err := strconv.Atoi(value)
	if err != nil {
		log.Fatal(err)
	}
	if numValue >= low && numValue <= high {
		return true
	}
	return false
}

func countValidFields(passInfo map[string]string) int {
	var validFields int = 0
	fmt.Println(passInfo)

	if validateString(passInfo["byr"], "[0-9]{4}", 4) && validRange(passInfo["byr"], 1920, 2002) {
		validFields += 1
		fmt.Println("byr valid")
	}
	if validateString(passInfo["iyr"], "[0-9]{4}", 4) && validRange(passInfo["iyr"], 2010, 2020) {
		validFields += 1
		fmt.Println("iyr valid")
	}
	if validateString(passInfo["eyr"], "[0-9]{4}", 4) && validRange(passInfo["eyr"], 2020, 2030) {
		validFields += 1
		fmt.Println("eyr valid")
	}
	if validateString(passInfo["hgt"], "[0-9]{3}cm", 5) && validRange(strings.Trim(passInfo["hgt"], "cm"), 150, 193) ||
		validateString(passInfo["hgt"], "[0-9]{2}in", 4) && validRange(strings.Trim(passInfo["hgt"], "in"), 59, 76) {
		validFields += 1
		fmt.Println("hgt valid")
	}
	if validateString(passInfo["hcl"], "BV[0-9]{6}", 8) || validateString(passInfo["hcl"], "|BV[a-f]{6}", 8) {
		validFields += 1
		fmt.Println("hcl valid")
	}
	if validateString(passInfo["pid"], "[0-9]{9}", 9) {
		validFields += 1
		fmt.Println("pid valid")
	}
	tmpStr := [7]string{"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}
	matches := 0
	for _, tmps := range tmpStr {
		if validateString(passInfo["ecl"], tmps, 3) {
			matches += 1
		}
	}
	if matches == 1 {
		validFields += 1
		fmt.Println("ecl valid")
	}
	return validFields
}

func validPassports(passports []string) int {
	var validPass int = 0

	for _, passportRaw := range passports {
		reg, err := regexp.Compile("[^A-Za-z0-9: ]")
		if err != nil {
			log.Fatal(err)
		}
		passport := reg.ReplaceAllString(passportRaw, "BV")
		//fmt.Println(passport)
		passData := rex.FindAllStringSubmatch(passport, -1)
		passInfo := make(map[string]string)
		for _, kv := range passData {
			k := kv[1]
			v := kv[2]
			passInfo[k] = v
			//fmt.Printf("f: %s ", passInfo[k])
		}
		//fmt.Printf("%d\n", len(passInfo))

		validFields := countValidFields(passInfo)
		fmt.Printf("validFields: %d\n", validFields)
		_, ok := passInfo["cid"]
		if len(passInfo) == 8 && validFields == 7 {
			validPass += 1
		} else if len(passInfo) == 7 && ok == false && validFields == 7 {
			validPass += 1
		}
	}
	//fmt.Printf("data %v", data)
	return validPass
}

func main() {

	var data []string
	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		fmt.Printf("f = %s\n", *filePtr)
		data = readFile(*filePtr)
		//fmt.Printf("%v\n", data)
		validPass := validPassports(data)
		fmt.Printf("validPassports: %d\n", validPass)
	}
}
