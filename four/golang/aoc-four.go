package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strings"
)

var rex = regexp.MustCompile("(\\w+):(\\w+)")

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	//t := strings.Map(func(r rune) rune {
	//	if r > unicode.MaxASCII {
	//		return -1
	//	}
	//	return r
	//}, s)
	text := strings.Split(string(content), "\n\n")
	for i := range text {
		str := strings.Replace(text[i], "\n", " ", -1)
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, str)
	}

	return data
}

func validPassports(passports []string) int {
	var validPass int = 0

	for _, passportRaw := range passports {
		reg, err := regexp.Compile("[^A-Za-z0-9: ]")
		if err != nil {
			log.Fatal(err)
		}
		passport := reg.ReplaceAllString(passportRaw, "badv")
		fmt.Println(passport)
		passData := rex.FindAllStringSubmatch(passport, -1)
		passInfo := make(map[string]string)
		for _, kv := range passData {
			k := kv[1]
			v := kv[2]
			passInfo[k] = v
			//fmt.Printf("f: %s ", passInfo[k])
		}
		fmt.Println(passInfo)
		//fmt.Printf("%d", len(passInfo))
		_, ok := passInfo["cid"]
		if len(passInfo) == 8 {
			validPass += 1
		} else if len(passInfo) == 7 && ok == false {
			validPass += 1
		}
	}
	//fmt.Printf("data %v", data)
	return validPass
}

func main() {

	var data []string
	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		fmt.Printf("f = %s\n", *filePtr)
		data = readFile(*filePtr)
		//fmt.Printf("%v\n", data)
		validPass := validPassports(data)
		fmt.Printf("validPassports: %d\n", validPass)
	}
}
