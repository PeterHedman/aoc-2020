package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	data = strings.Split(string(content), "\n")

	return data[0 : len(data)-1]
}

func convert2int(data string, high rune, low rune) int {
	var value byte = 0
	maxIter := len(data)
	for i, char := range data {
		switch char {
		case high:
			// set bit
			value |= (1 << (maxIter - 1 - i))
		case low:
			//clear bit not really needed
			value &^= (1 << (maxIter - 1 - i))

		default:
			fmt.Printf("this is unexpected")
		}
		fmt.Printf("value: %d\n", value)
	}
	return int(value)
}

func findMySeatID(data []string) int {
	var mySeatID int = 0
	var rowBytes string = "BF"
	var columnBytes string = "RL"
	var seats [128][8]bool

	// seats at fron and back are missing
	// seats with myID +1 and myID -1 exists = column +/- 1
	for _, bC := range data {
		row := strings.Trim(bC, "LR")
		column := strings.Trim(bC, "FB")
		rowNr := convert2int(row, rune(rowBytes[0]), rune(rowBytes[1]))
		columnNr := convert2int(column, rune(columnBytes[0]), rune(columnBytes[1]))
		seats[rowNr][columnNr] = true
	}
	for r := 1; r < 127; r += 1 {
		for c := 1; c < 7; c += 1 {
			if seats[r][c] == false {
				seatID := (r*8 + c)
				if seats[r][c+1] == true && seats[r][c-1] == true {
					mySeatID = seatID
					return mySeatID
				}
			}
		}
	}
	return mySeatID
}

func main() {

	var data []string
	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		fmt.Printf("f = %s\n", *filePtr)
		data = readFile(*filePtr)
		fmt.Printf("%v\n", data)
		mySeatID := findMySeatID(data)
		fmt.Printf("mySeatID: %d\n", mySeatID)
	}
}
