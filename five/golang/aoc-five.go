package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	data = strings.Split(string(content), "\n")

	return data[0 : len(data)-1]
}

func convert2int(data string, high rune, low rune) int {
	var value byte = 0
	maxIter := len(data)
	for i, char := range data {
		switch char {
		case high:
			// set bit
			value |= (1 << (maxIter - 1 - i))
		case low:
			//clear bit not really needed
			value &^= (1 << (maxIter - 1 - i))

		default:
			fmt.Printf("this is unexpected")
		}
		fmt.Printf("value: %d\n", value)
	}
	return int(value)
}

func findHighestSeatID(data []string) int {
	var highestSeatID int = 0
	var rowBytes string = "BF"
	var columnBytes string = "RL"

	for _, bC := range data {
		row := strings.Trim(bC, "LR")
		column := strings.Trim(bC, "FB")
		//fmt.Printf("row: %s\ncolumn: %s\n", row, column)
		rowNr := convert2int(row, rune(rowBytes[0]), rune(rowBytes[1]))
		//fmt.Printf("rowNr: %d\n", rowNr)
		columnNr := convert2int(column, rune(columnBytes[0]), rune(columnBytes[1]))
		//fmt.Printf("columnNr: %d\n", columnNr)
		seatID := (rowNr*8 + columnNr)
		//fmt.Printf("seatID: %d\n", seatID)
		if seatID > highestSeatID {
			highestSeatID = seatID
		}
	}

	return highestSeatID
}

func main() {

	var data []string
	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		fmt.Printf("f = %s\n", *filePtr)
		data = readFile(*filePtr)
		fmt.Printf("%v\n", data)
		highestSeatID := findHighestSeatID(data)
		fmt.Printf("highestSeatID: %d\n", highestSeatID)
	}
}
