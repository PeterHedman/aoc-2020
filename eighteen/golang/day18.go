package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n")
	for _, raw := range text {
		reg2 := regexp.MustCompile("[^\\+[0-9]+\\*\\(\\) ")
		str := reg2.ReplaceAllString(raw, "")
		data = append(data, str)
	}
	return data[0 : len(data)-1]
}

func oddMath2(expr string) (result int) {
	result = 0
	t := ""
	for _, e := range strings.Split(expr, " ") {
		if e == "+" || e == "*" {
			t = e
		} else {
			v, _ := strconv.Atoi(e)
			if t == "+" {
				result += v
			} else if t == "*" {
				result *= v
			} else {
				result = v
			}
			t = ""
		}
	}
	return result
}

// based on https://raw.githubusercontent.com/Peter554/adventofcode/master/2020/day18/main.go
func oddMath1(expr string, prob int) int {
	res := regexp.MustCompile(`\([^\(\)]+\)`)
	for res.MatchString(expr) {
		expr = res.ReplaceAllStringFunc(expr, func(str string) string {
			return strconv.Itoa(oddMath1(str[1:len(str)-1], prob))
		})
	}

	if prob == 2 {
		res = regexp.MustCompile(`\d+ \+ \d+`)
		for res.MatchString(expr) {
			expr = res.ReplaceAllStringFunc(expr, func(str string) string {
				return strconv.Itoa(oddMath2(str))
			})
		}
	}

	return oddMath2(expr)
}

func solution1(data []string) int {
	var res int = 0
	for _, expr := range data {
		res += oddMath1(expr, 1)
	}
	return res
}

func solution2(data []string) int {
	var res int = 0
	for _, expr := range data {
		res += oddMath1(expr, 2)
	}
	return res
}

func main() {

	var data []string
	filePtr := flag.String("f", "", "a filepath string")
	flag.Parse()

	if *filePtr != "" {
		data = readFile(*filePtr)
		//fmt.Println(data)
		solution1 := solution1(data)
		fmt.Printf("Solution1: %d\n", solution1)
		solution2 := solution2(data)
		fmt.Printf("Solution2: %d\n", solution2)
	}
}
