package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func readFile(file string) []int {
	var data []int

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n")
	for _, rawNr := range text {
		reg2 := regexp.MustCompile("[^0-9]+")
		str := reg2.ReplaceAllString(rawNr, "")
		val, e := strconv.Atoi(str)
		if e != nil {
			fmt.Println(e)
		}
		data = append(data, val)
	}
	return data[0 : len(data)-1]
}

//func isNrValid(data int, i, preamble)

func solution1(data []int, preamble int) int {
	for i := preamble; i < len(data); i++ {
		nr := data[i]
		nrOk := false
		//nrOk = isNrValid(data, i, preamble)
		for j := (i - preamble); j < i; j++ {
			for k := (i - preamble); k < i; k++ {
				sum := data[k] + data[j]
				fmt.Printf("%d %d %d %d\n", data[i], data[j], data[k], sum)
				if j != k && sum == nr {
					nrOk = true
				}
				if j == i-1 && k == i-1 && nrOk == false {
					return nr
				}
				fmt.Printf("%d %d %d\n", i, j, k)
			}
		}
	}
	fmt.Printf("All numbers valid")
	return -1
}

func findMax(data []int) int {
	max := data[0]
	for _, val := range data {
		if val > max {
			max = val
		}
	}
	return max
}
func findMin(data []int) int {
	min := data[0]
	for _, val := range data {
		if val < min {
			min = val
		}
	}
	return min
}

func calcSum(data []int) int {
	sum := 0
	for i := 0; i < len(data); i++ {
		sum += data[i]
	}
	return sum
}

func solution2(data []int, value int) int {
	var result int = 0
	for i := 0; i < len(data)-1; i++ {
		for j := i + 1; j < len(data); j++ {
			sum := calcSum(data[i:j])
			if sum == value {
				result = findMax(data[i:j]) + findMin(data[i:j])
				return result
			}
		}
	}
	return result
}

func main() {

	var data []int
	filePtr := flag.String("f", "", "a filepath string")
	preambleInt := flag.Int("p", 25, "preamble")
	flag.Parse()

	if *filePtr != "" && *preambleInt != 0 {
		fmt.Printf("f = %s\n", *filePtr)
		fmt.Printf("p = %d\n", *preambleInt)
		data = readFile(*filePtr)
		//fmt.Printf("%v\n", data)
		solution1 := solution1(data, *preambleInt)
		fmt.Printf("Solution1: %d\n", solution1)
		//solution := 90433990
		//solution := 127
		solution2 := solution2(data, solution1)
		fmt.Printf("Solution2: %d\n", solution2)
	}
}
