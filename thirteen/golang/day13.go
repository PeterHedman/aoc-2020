package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n")
	for _, rawNr := range text {
		reg2 := regexp.MustCompile("[^0-9,x]+")
		str := reg2.ReplaceAllString(rawNr, "")
		data = append(data, str)
	}
	return data[0 : len(data)-1]
}

func getDepartureTime(data []string) int {
	reg2 := regexp.MustCompile("[^0-9]+")
	str := reg2.ReplaceAllString(data[0], "")
	val, e := strconv.Atoi(str)
	if e != nil {
		fmt.Println(e)
	}
	return val
}

func getBusses(data []string) []int {
	var result []int
	busses := strings.Split(data[1], ",")
	for _, bus := range busses {
		reg2 := regexp.MustCompile("[^x0-9]+")
		bus := reg2.ReplaceAllString(bus, "")
		if strings.Compare(bus, "x") != 0 {
			val, e := strconv.Atoi(bus)
			if e != nil {
				fmt.Println(e)
			}
			result = append(result, val)
		}
	}
	return result
}

//data contains on line 1 earliest departure
// 2nd line contains BUS ID's and x'x
// x's can be ignored
// BUS IS also gives each minute a bus departs and returns
// starting from 0
func solution1(data []string) int {
	var result int = 0
	var busses []int
	var waitForBus bool = true
	var myBus int = 0

	sTime := getDepartureTime(data)
	busses = getBusses(data)
	time := sTime
	//fmt.Println(time)
	//fmt.Println(busses)
	for waitForBus == true {
		for _, bus := range busses {
			if time%bus == 0 {
				myBus = bus
				waitForBus = false
			}
		}
		time++
	}
	fmt.Printf("time:%d sTime: %d busId:%d\n", time, sTime, myBus)
	result = (time - sTime - 1) * myBus
	return result
}

func getBusses2(data string) []int {
	var result []int
	busses := strings.Split(data, ",")
	for _, bus := range busses {
		reg2 := regexp.MustCompile("[^x0-9]+")
		bus := reg2.ReplaceAllString(bus, "")
		if strings.Compare(bus, "x") != 0 {
			val, e := strconv.Atoi(bus)
			if e != nil {
				fmt.Println(e)
			}
			result = append(result, val)
		} else {
			result = append(result, 0)
		}
	}
	return result
}

func getNrOfElements(data []int) int {
	var iter int = 0
	for _, bus := range data {
		if bus != 0 {
			iter++
		}
	}
	return iter
}

// find solution where busses go within pos nr of minutes from each other
// time must be % with each bus but time differs somewhat between each
// Once you have found match for 2 busses start multiplying with
// minsta gemensamma nämnare LCD?
// since next match must still have the previous
func solution2(data string) int {
	busses := getBusses2(data)
	fmt.Println(busses)
	time := 100000000000000
	//time := 0
	iter := 1
	elem := getNrOfElements(busses)
	fmt.Println(elem)
	fmt.Println(iter)
	fmt.Println(busses)

	for elem > 0 {
		for i, bus := range busses {
			if bus != 0 && (time+i)%bus == 0 {
				//a new match
				elem--
				busses[i] = 0
				iter *= bus
			} else if bus == 0 {
				continue
			} else { //skip to next loop enough in the future for previous loop to repeat
				time += iter
				break
			}
		}
	}
	fmt.Println(iter)

	return time
}

func main() {

	var data []string
	filePtr := flag.String("f", "", "a filepath string")
	preambleInt := flag.Int("p", 3, "device")
	flag.Parse()

	if *filePtr != "" && *preambleInt != 0 {
		data = readFile(*filePtr)
		//fmt.Println(data)
		solution1 := solution1(data)
		fmt.Printf("Solution1: %d\n", solution1)
		solution2 := solution2(data[1])
		fmt.Printf("Solution2: %d\n", solution2)
	}
}
