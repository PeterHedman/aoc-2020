package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n")
	for _, raw_rule := range text {
		str := strings.Replace(raw_rule, "bags contain", ":", -1)
		str = strings.Replace(str, "no other bags", "", -1)
		str = strings.Replace(str, "no other", "", -1)
		str = strings.Replace(str, "bags", ":", -1)
		str = strings.Replace(str, "bag", ":", -1)
		str = strings.Replace(str, "::", "", -1)
		str = strings.Replace(str, ",", "", -1)
		str = strings.Replace(str, ".", "", -1)
		str = strings.Replace(str, " ", "", -1)
		str = strings.TrimRight(str, ":")
		//reg := regexp.MustCompile("[0-9]+")
		//str = reg.ReplaceAllString(str, "")
		reg2 := regexp.MustCompile("[^A-Za-z0-9: ]+")
		str = reg2.ReplaceAllString(str, "")
		data = append(data, str)
	}

	return data
}

func getValue(bag string) int {
	fmt.Printf("valyes: %s *", bag[0])
	val, e := strconv.Atoi(string(bag[0]))
	if e != nil {
		fmt.Println(e)
	}
	fmt.Printf("val %d -", val)
	return val
}

func getChilds(rules []string, rootBag string) []string {
	var res []string
	for _, rule := range rules {
		bags := strings.Split(rule, ":")
		for _, bag := range bags {
			if strings.Compare(rootBag, bag) == 0 {
				fmt.Printf("child: %v\n", bags)
				return bags
			}
		}
	}
	return res
}

func solveProblem2(data []string, bagColor string) int {
	var result []string
	var values []int
	result = append(result, bagColor)
	//for strings.Compare(bag, "last") != 0 {
	i := 0
	bag := bagColor
	for len(result) > i {
		//for i := 0; i < 5; i++ {
		bags := getChilds(data, result[i])
		val := 0
		//getValue(bag2)
		for _, bag2 := range bags {
			if strings.Compare(bag2, result[i]) != 0 {
				val += val + getValue(bag2)
				bag = bag2[1:len(bag2)]
				result = append(result, bag)
				fmt.Printf("%s\n", bag)
			}
		}
		values = append(values, val)
		i++
		//fmt.Printf("value: %d -- %s", val, bag)
	}
	sum := 0
	//for j := 0; j < len(values)-1; j += 1 {
	//	sum += values[j] * j
	//}
	fmt.Printf("values: %v\n", values)
	fmt.Printf("%v\n", result)
	nrOfBags := sum

	//we included first shinygold
	return nrOfBags
}

func main() {

	var data []string
	filePtr := flag.String("f", "", "a filepath string")
	bagColor := flag.String("c", "", "color of bag")
	flag.Parse()

	if *bagColor != "" && *filePtr != "" {
		fmt.Printf("f = %s\n", *filePtr)
		data = readFile(*filePtr)
		fmt.Printf("%v\n", data)
		solution := solveProblem2(data, *bagColor)
		fmt.Printf("Solution: %d\n", solution)
	}
}
