package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strings"
)

func readFile(file string) []string {
	var data []string

	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	text := strings.Split(string(content), "\n")
	for _, raw_rule := range text {
		str := strings.Replace(raw_rule, "bags contain", ":", -1)
		str = strings.Replace(str, "no other bags", "", -1)
		str = strings.Replace(str, "no other", "", -1)
		str = strings.Replace(str, "bags", ":", -1)
		str = strings.Replace(str, "bag", ":", -1)
		str = strings.Replace(str, "::", "", -1)
		str = strings.Replace(str, ",", "", -1)
		str = strings.Replace(str, ".", "", -1)
		str = strings.Replace(str, " ", "", -1)
		str = strings.TrimRight(str, ":")
		reg := regexp.MustCompile("[0-9]+")
		str = reg.ReplaceAllString(str, "")
		reg2 := regexp.MustCompile("[^A-Za-z0-9: ]+")
		str = reg2.ReplaceAllString(str, "")
		data = append(data, str)
	}

	return data
}

func getTopBags(data []string, color string) []string {
	var topBags []string

	for _, rule := range data {
		match, err := regexp.MatchString(color, rule)
		if err != nil {
			log.Fatal(err)
		}
		bags := strings.Split(rule, ":")
		//fmt.Println(bags)
		if match == true && strings.Compare(bags[0], "shinygold") != 0 {
			topBags = append(topBags, bags[0])
		}
	}
	//fmt.Println("top", topBags)
	return topBags
}

func bagNew(searched []string, bag string) bool {
	for _, sbag := range searched {
		if strings.Compare(sbag, bag) == 0 {
			return false
		}
	}
	return true
}

func solveProblem(data []string, bagColor string) int {
	var result []string
	var searchedBags []string

	result = append(result, bagColor)
	i := 0
	for len(result) > i {
		bags := getTopBags(data, result[i])
		for _, bag := range bags {
			if bagNew(searchedBags, bag) == true {
				searchedBags = append(searchedBags, bag)
				result = append(result, bag)
			}
		}
		i++
	}
	fmt.Println(result)
	//we included first shinygold
	return len(result) - 1
}

func main() {

	var data []string
	filePtr := flag.String("f", "", "a filepath string")
	bagColor := flag.String("c", "", "color of bag")
	flag.Parse()

	if *bagColor != "" && *filePtr != "" {
		fmt.Printf("f = %s\n", *filePtr)
		data = readFile(*filePtr)
		//fmt.Printf("%v\n", data)
		solution := solveProblem(data, *bagColor)
		fmt.Printf("Solution: %d\n", solution)
	}
}
